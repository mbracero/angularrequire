define(['./module'], function (controllers) {
'use strict';
controllers.controller('AboutCtrl',
	[function ($scope) {
		$scope.nombre = "Pepe";
		$scope.awesomeThings = [
	      'HTML5 Boilerplate - about',
	      'AngularJS - about',
	      'Karma - about'
	    ];
	}]);
});