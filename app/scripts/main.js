/**
* configure RequireJS
* prefer named modules to long paths, especially for version mgt
* or 3rd party libraries
*/
require.config({
	paths: {
		'angular': '../bower_components/angular/angular',
		'angular-route': '../bower_components/angular-route/angular-route',
		'jquery': '../bower_components/jquery/dist/jquery',
		'jquery.bootstrap': '../bower_components/bootstrap/dist/js/bootstrap',
		'domReady': '../bower_components/requirejs-domready/domReady'
	},
	/**
	* for libs that either do not support AMD out of the box, or
	* require some fine tuning to dependency mgt'
	*/
	shim: {
		'angular': {
			exports: 'angular'
		},
		'angular-route': {
			deps: ['angular']
		},
		'jquery.bootstrap': {
			deps: ['jquery']
		}
	},
	deps: [
		// kick start application... see bootstrap.js
		'./bootstrap'
	]
});