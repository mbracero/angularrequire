/**
* Defines the main routes in the application.
* The routes you see here will be anchors '#/' unless specifically configured otherwise.
*/
define(['./app'], function (app) {
	'use strict';
	return app.config(['$routeProvider', function ($routeProvider) {
		$routeProvider.when('/', {
			templateUrl: 'views/main.html',
        	controller: 'MainCtrl'
		});
		$routeProvider.when('/about', {
			templateUrl: 'views/about.html',
        	controller: 'AboutCtrl'
		});
		$routeProvider.otherwise({
			redirectTo: '/'
		});
	}]);
});