## angularrequire

**Importante. Cada ver que se arranque el servidor (grunt cli) se modifica index.html (./app/)** debe de quedar como sigue:

(...)
<!--    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/angular/angular.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script src="bower_components/angular-route/angular-route.js"></script>-->
    <script src="bower_components/requirejs/require.js" data-main="scripts/main.js"></script>
(...)

yo + grunt + bower + angular + twitter bootstrap + require

Realizando pruebas con generator-angular (https://github.com/yeoman/generator-angular) + require

Teniendo en cuenta http://www.startersquad.com/angularjs-requirejs/
	https://github.com/StarterSquad/startersquad.com/tree/master/examples/angularjs-requirejs-1
	https://github.com/StarterSquad/startersquad.com/tree/master/examples/angularjs-requirejs-2

Que realmente NO FUNCIONA (características de angularjs), ni aunque se obtenga el proyecto de origen. Ver página about.

Añadido en bower.json (dependencia en cliente) "requirejs" y "requirejs-domready".

Configurando twitter-bootstrap en main.js (./app/scripts/). Como referencia http://stackoverflow.com/questions/16259098/cant-load-bootstrap-with-requirejs, https://github.com/sudo-cm/requirejs-bootstrap-demo/blob/master/assets/js/app.js.

